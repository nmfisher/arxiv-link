import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first' // in imports

@Injectable()
export class PaperService { 
    constructor(private http:Http) {}

    getMetadata(encodedUrl, fetchRelated) {
        var decoded = decodeURIComponent(encodedUrl);
        var split = decoded.split("/");
        var report = this.http.request('./assets/'+split[split.length-1]+".json").map(obj => { 
            obj = obj.json();
            if(fetchRelated) {
                this.resolve(obj["comments"]).subscribe(value => { 
                    obj["comments_resolved"] = value;
                    obj["commentsByMonth"] = this.groupByMonth(value);            
                });
                this.resolve(obj["related"]).subscribe(value => {
                    obj["related_resolved"] = value;
                });
                this.resolve(obj["repositories"]).subscribe(value => {
                    obj["repositories_resolved"] = value;
                });
            }
            return obj;
        }).catch(this.handleError).first();
        return report;
    }
    
    private handleError (error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            //const err = body.error || JSON.stringify(body);
            const err ="";
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.from([{related:[], related_resolved:[], repositories:[], repositories_resolved:[], comments:[], comments_resolved:[]}]); 
    }
    
    resolve(values: Array<any>) {
        return Observable.forkJoin(values.map( val => {return this.getMetadata(val, false) }));
    }
    
    groupByMonth(value: Array<any>) {
        for(let i in value) {
            let obj = value[i];
            var d = new Date(0); 
            d.setUTCSeconds(obj['date']);
            obj['month'] = d.getFullYear().toString() + "-" +  + d.getMonth().toString();
        }
        return this.groupBy(value, "month")
    }

    groupBy(value: Array<any>, field: string): Array<any> {
        const groupedObj = value.reduce((prev, cur)=> {
          if(!prev[cur[field]]) {
            prev[cur[field]] = [cur];
          } else {
            prev[cur[field]].push(cur);
          }
          return prev;
        }, {});
        return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key] }));
    }
}