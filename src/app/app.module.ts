import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { RouterModule }   from '@angular/router';
import { AppComponent }        from './app.component';
import { UrlFormComponent }        from './url-form.component';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import { PaperService }        from './paper.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    NgbModule.forRoot(),
    RouterModule.forRoot([
      {
        path: 'paper',
        component: AppComponent
      }
    ])
  ],
  declarations: [
    AppComponent,
    UrlFormComponent
  ],
  providers: [
    PaperService,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {

}