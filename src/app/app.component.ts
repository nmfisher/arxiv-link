import {Router, ActivatedRoute, Params} from '@angular/router';
import {OnInit, OnDestroy, Component, ViewChild} from '@angular/core';
import {PaperService} from './paper.service';
import {UrlFormComponent} from './url-form.component';
import {DomSanitizer} from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {ElementRef,Renderer2} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { URLSearchParams } from "@angular/http"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

    @ViewChild('messageEmail') messageEmail:ElementRef;
    @ViewChild('messageText') messageText:ElementRef;
    @ViewChild('messageSuccess') messageSuccess:ElementRef;
    @ViewChild('shareEmail') shareEmail:ElementRef;
    @ViewChild('shareSuccess') shareSuccess:ElementRef;

    constructor(private route: ActivatedRoute, 
                    private sanitizer: DomSanitizer,
                        private _paperService: PaperService,
                            private location:Location,
                                private http:Http) { }
    
    paper = null;
    rawPaperUrl = null;
    sanitizedPaperUrl = null;
    collapse = {repositories:true, comments:true, related:true, share:true, contact:true, about:true, faq:false};
        
    public toggle(item) {
        this.collapse[item] = !this.collapse[item]
        for(var other in this.collapse) {
            if(other != item) {
                this.collapse[other] = true;
            }
        }
    }
    
    public tweet() {
        window.open("https://twitter.com/intent/tweet?text="+encodeURIComponent('http://arxiv-link.org/?url='+this.rawPaperUrl+' @AviniumAU @IndustryGovAU')+'&source=webclient',"Twitter", "height=285,width=550,resizable=1");
    }
    
    public message(event) {
        let data = new URLSearchParams();
        data.append("email", this.messageEmail.nativeElement.value);
        data.append("message", "\n\n (VIA ARXIV-LINK.ORG) \n\n " + this.messageText.nativeElement.value);        
        this.http.post('http://www.avinium.com/contact.php', data).map((res:any) => console.log(res.json()))
            .catch((error:any) => Observable.throw(error.json().error || 'Server error')).first().subscribe();
        this.messageSuccess.nativeElement.style.display = "block";
    }
    
    public share(event) {
        let data = new URLSearchParams();
        data.append("email", this.shareEmail.nativeElement.value);
        data.append("message", "\n\n (VIA ARXIV-LINK.ORG) \n\n share with : " + this.shareEmail.nativeElement.value);        
        this.http.post('http://www.avinium.com/contact.php', data).map((res:any) => console.log(res.json()))
            .catch((error:any) => Observable.throw(error.json().error || 'Server error')).first().subscribe();
        this.shareSuccess.nativeElement.style.display = "block";
    }

    ngOnInit() {
      this.route
          .queryParams
          .subscribe(params => {
                if(params["url"]) {
                    window.onload = null;
                    this.rawPaperUrl = params["url"];
                    this.sanitizedPaperUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.rawPaperUrl);  
                    let paper = this._paperService.getMetadata(params["url"], true);
                    paper.subscribe(value => { 
                        this.paper = value;
                    });
                } else {
                    window.onload = function(e) {
                        console.log("Window loaded, rendering animation...");
                         new FractalRenderer({animation_points:350,
                                        max_depth: 4,
                                        line_color:"#E45735",
                                        line_shadow_color:"#E45735", 
                                        base_length:2,
                                        customizable:true,
                                        line_blur:1,
                                        internal_angle:(120 / 360) * (2 * Math.PI), skew_angle:(0 / 360) * (2 * Math.PI),
                                        origin : new Point(30,190),
                                        initial_axis_angle: 0,
                                        canvas_width:275,
                                        canvas_height:200})       
                    }
                }
          });
    }
}


export class Paper {
  id: number;
  name: string;
}
